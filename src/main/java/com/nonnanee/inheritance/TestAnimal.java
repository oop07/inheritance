/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.inheritance;

/**
 *
 * @author nonnanee
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "BlackWhite");
        dang.speak();
        dang.walk();
        
        Dog to = new Dog("To", "Orange");
        to.speak();
        to.walk();
        
        Dog bat = new Dog("Bat", "BlackWhite");
        bat.speak();
        bat.walk();
        
        Dog mome = new Dog("Mome", "BlackWhite");
        mome.speak();
        mome.walk();
        
        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();

        Duck zom = new Duck("Zom", "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck gabgab = new Duck("GabGab", "BlackOrange");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();


        System.out.println("Zom is Animal: " + (zom instanceof Animal));
        System.out.println("Zom is Duck: " + (zom instanceof Duck));
        System.out.println("Zom is Cat: " + (zom instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));

        Animal[] animals = {dang, zero, zom};
        for (int i = 0; i < animals.length; i++) {
            animals[i].walk();
            animals[i].speak();
            if (animals[i] instanceof Duck) {
                Duck duck = (Duck) animals[i];
                duck.fly();
            }
        }
    }
}
